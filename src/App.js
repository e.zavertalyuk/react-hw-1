
import './App.css';
import React, { useEffect } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Header from './components/header/header';
import { AppRoutes } from './AppRoutes/appRoutes';
import { useDispatch } from "react-redux"
import { useSelector } from 'react-redux'
import { getProductsAsync } from './redux/actions/products';
import { toogleModal } from './redux/actions/modal';
import { selectItem } from './redux/actions/selectedItem';
export default function App() {
   const dispatch = useDispatch()
   const products = useSelector((state) => state.products)

   useEffect(() => {
      dispatch(getProductsAsync())
   }, [dispatch]);


   const handleClickAddToChart = (id) => {
      const item = products.find((item) => item.id === id);
      dispatch(selectItem(item))
      dispatch(toogleModal())
   };
   const handleClickAddToChartInCart = (id) => {
      alert('Товар уже в корзине!')
   };

   const handleRemoveToCart = (id) => {
      const item = products.find((item) => item.id === id);
      dispatch(selectItem(item))
      dispatch(toogleModal())
   };

   return (
      <Router>
         <Header />
         {products?.length ?
            <div className='main-wrap'>
               <AppRoutes
                  handleClickAddToChartInCart={(id) => () => { handleClickAddToChartInCart(id) }}
                  handleRemoveToCart={(id) => () => { handleRemoveToCart(id) }}
                  handleClickAddToChart={(id) => () => { handleClickAddToChart(id) }}
               />
            </div> : <div>Loading...</div>}
      </Router >
   )
}



