let modalObj = [
   {
      id: 1,
      header: "Do you want to delete this file?",
      text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
      closeButton: true
   },
   {
      id: 2,
      header: "Do you want to delete this video/music?",
      text: "Modal text 2, custom",
      closeButton: false
   }

]
export default modalObj