import { useDispatch, useSelector } from "react-redux"
import ItemWrapper from "../components/ItemWrapper"
import Modal from "../components/modal"
import Form from "../components/form"
import './home.scss'
import { removeFromCartItems } from '../redux/actions/cartItems';
import { removeItem } from '../redux/actions/selectedItem';
import { toogleModal } from '../redux/actions/modal';
export default function CartItems(props) {
   const dispatch = useDispatch()
   const { cartItems } = useSelector((state) => state.cart)
   const { openModal } = useSelector((state) => state.modal)
   const { selectedItem } = useSelector((state) => state.selectItem)
   const handleClickModalInCart = (e, id) => {
      if (
         e.target.className === "back" ||
         e.target.classList.contains("cancel") ||
         e.target.classList.contains("close-button")
      ) {
         dispatch(toogleModal())
      } else if (e.target.classList.contains("confirm")) {
         dispatch(removeFromCartItems(selectedItem))
         dispatch(toogleModal())
         dispatch(removeItem())
      }
   }
   return (

      <>
         <div>
            {
               openModal ? <Modal closeButton onClick={handleClickModalInCart}
                  header={"Вы желаете удалить товар из корзины?"}
                  text={"Подтвердите действие!"}
                  imgPath={selectedItem.img}
                  actions={
                     <div className='btn-wrap'>
                        <button className="modal__content__btn confirm">Confirm</button>
                        <button className="modal__content__btn cancel">Cancel</button>
                     </div>
                  }
               /> : null
            }
            {cartItems.length === 0 ? <div className="nothingAdd">Nothing has been added</div> :
               <>
                  <h2 className="titleText">Our cart:</h2>
                  <div className="cartWrap">
                     <ItemWrapper
                        addToCart={props.addToCart}
                        items={cartItems}
                        delBtn
                        removeFromCart={props.removeFromCart}
                     />
                     <Form />
                  </div>
               </>}
         </div >
      </>
   )
}