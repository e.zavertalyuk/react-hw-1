
import './home.scss';
import Modal from '../components/modal';
import ItemWrapper from '../components/ItemWrapper';
import { useSelector, useDispatch } from 'react-redux'
import { addToCartItems } from '../redux/actions/cartItems';
import { removeItem } from '../redux/actions/selectedItem';
import { toogleModal } from '../redux/actions/modal';

export default function Home(props) {
   const { openModal } = useSelector((state) => state.modal)
   const products = useSelector((state) => state.products)
   const dispatch = useDispatch()

   const { selectedItem } = useSelector((state) => state.selectItem)
   const handleClickModal = (e) => {
      if (
         e.target.className === "back" ||
         e.target.classList.contains("cancel") ||
         e.target.classList.contains("close-button")
      ) {

      } else if (e.target.classList.contains("confirm")) {

         dispatch(addToCartItems(selectedItem))
         dispatch(removeItem())
      }
      dispatch(toogleModal())
   }
   return (
      <div className='main-wrap'>
         {
            openModal ? <Modal closeButton onClick={handleClickModal}
               header={"Добавить выбранный товар в корзину?"}
               text={` Цена товара ${selectedItem.price} UAH`}
               imgPath={selectedItem.img}
               actions={
                  <div className='btn-wrap'>
                     <button className="modal__content__btn confirm">Confirm</button>
                     <button className="modal__content__btn cancel">Cancel</button>
                  </div>
               }
            /> : null
         }

         <ItemWrapper
            items={products}
            removeFromCart={props.removeFromCart}
            addToCart={props.addToCart} />
      </div>
   )
}


