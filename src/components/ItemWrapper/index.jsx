
import ProductItem from "../ProductItem";
import './item.scss'
import PropTypes from "prop-types";
import React from 'react';
import { useSelector } from "react-redux";
function ItemWrapper(props) {
   const { items, addToCart, removeFromCart } = props;
   const { favItems } = useSelector((state) => state.fav)

   return (
      <div className="productItem">
         {items.map((item) => (
            <ProductItem
               delBtn={props.delBtn}
               className='item'
               key={item.id}
               item={item}
               favourite={favItems.find((favoriteItem) => favoriteItem.id === item.id) ? true : false}
               removeFromCart={removeFromCart(item.id)}
               addToCart={addToCart(item.id)}
            />
         ))}
      </div>
   );
}

ItemWrapper.propTypes = {
   addToFavClick: PropTypes.func,
   removeFromFavClick: PropTypes.func,
   onClick: PropTypes.func,
   favoriteItems: PropTypes.array,
   items: PropTypes.array,
};

ItemWrapper.defaultProps = {
   items: [],
   favoriteItems: [],
};

export default ItemWrapper;
