import { useFormik } from "formik";
import { useDispatch } from "react-redux"
import './style.scss'
import { clearCart } from "../../redux/actions/cartItems";
import { usersSchema } from "../../schemas";
import { PatternFormat } from "react-number-format";
export default function Form() {
   const dispatch = useDispatch()
   const cartForm = useFormik({
      initialValues: {
         firstName: '',
         lastName: '',
         userAge: '',
         delivery: '',
         phoneNumber: '',
      },
      validationSchema: usersSchema,
      onSubmit: (values) => {
         dispatch(clearCart(values))
         alert('Спасибо за покупку!')
      }
   })
   return (
      <form className="form" onSubmit={cartForm.handleSubmit}>
         <h3 className="formTitle">Delivery form:</h3>
         <input className={cartForm.errors.firstName && cartForm.touched.firstName ? 'inputError' : 'formInput'}
            onBlur={cartForm.handleBlur}
            type="text" id="firstName" name="firstName" value={cartForm.values.firstName} onChange={cartForm.handleChange} placeholder="Your First Name" />
         {cartForm.errors.firstName && cartForm.touched.firstName && (
            <label htmlFor="firstName" className="inputErrorLabel">{<span className="errorMessage">{cartForm.errors.firstName}</span>}</label>
         )}

         <input className={cartForm.errors.lastName && cartForm.touched.lastName ? 'inputError' : 'formInput'}
            onBlur={cartForm.handleBlur}
            type="text" id="lastName" name="lastName" value={cartForm.values.lastName} onChange={cartForm.handleChange} placeholder="Your Last Name" />
         {cartForm.errors.lastName && cartForm.touched.lastName && (
            <label htmlFor="lastName" className="inputErrorLabel">{<span className="errorMessage">{cartForm.errors.lastName}</span>}</label>
         )}

         <input className={cartForm.errors.userAge && cartForm.touched.userAge ? 'inputError' : 'formInput'}
            onBlur={cartForm.handleBlur}
            type="number" id="userAge" name="userAge" value={cartForm.values.userAge} onChange={cartForm.handleChange} placeholder="Your Age" />
         {cartForm.errors.userAge && cartForm.touched.userAge && (
            <label htmlFor="userAge" className="inputErrorLabel">{<span className="errorMessage">{cartForm.errors.userAge}</span>}</label>
         )}

         <input className={cartForm.errors.delivery && cartForm.touched.delivery ? 'inputError' : 'formInput'}
            onBlur={cartForm.handleBlur}
            type="text" id="delivery" name="delivery" value={cartForm.values.delivery} onChange={cartForm.handleChange} placeholder="Your delivery" />
         {cartForm.errors.delivery && cartForm.touched.delivery && (
            <label htmlFor="delivery" className="inputErrorLabel">{<span className="errorMessage">{cartForm.errors.delivery}</span>}</label>
         )}

         <PatternFormat
            className={cartForm.errors.phoneNumber && cartForm.touched.phoneNumber ? 'inputError' : 'formInput'}
            id="phoneNumber"
            name="phoneNumber"
            onBlur={cartForm.handleBlur}
            type='tel'
            format="+### (##) ### ## ##"
            mask="#"
            value={cartForm.values.phoneNumber || '380'}
            onValueChange={(values) => {
               cartForm.setFieldValue('phoneNumber', values.formattedValue);
            }}
         >
         </PatternFormat>

         {
            cartForm.errors.phoneNumber && cartForm.touched.phoneNumber && (
               <label htmlFor="phoneNumber" className="inputErrorLabel">
                  {<span className="errorMessage">{cartForm.errors.phoneNumber}</span>}
               </label>
            )
         }

         <button className="formBtn" type="submit">Checkout</button>
      </form >
   );
}