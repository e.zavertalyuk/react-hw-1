import React from "react";
import './style.scss';
export default function Button({ backgroundColor, text, onClickFn, modalId, className }) {
   return (
      <>
         <button data-id={modalId} className={className}
            style={{ backgroundColor, margin: '35px' }}
            onClick={onClickFn}
         >{text}</button>
      </>
   );
};


