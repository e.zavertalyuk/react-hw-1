
import PropTypes from "prop-types";
import './modal.scss';
export default function Modal({ onClick, closeButton, header, text, actions, onClose, imgPath }) {

   return (
      <>
         <div className="back" onClick={onClick}>
            <div className='modalBuy'>
               <h1 style={{ paddingLeft: '25px' }} className="modalHeader">
                  {header}
                  {closeButton && <button className="close-button" onClick={onClose}>✖</button>}
               </h1>
               <img style={{ marginTop: '10px', borderRadius: '7px' }} src={imgPath} alt={imgPath} width={200} height={150} />
               <p className="text">{text}</p>
               {actions}
            </div>
         </div>
      </>
   );
};

Modal.propTypes = {
   onClick: PropTypes.func,
   closeButton: PropTypes.bool,
   header: PropTypes.string,
   text: PropTypes.string,
   actions: PropTypes.element,
};

Modal.defaultProps = {
   text: '',
   closeButton: true,
};
