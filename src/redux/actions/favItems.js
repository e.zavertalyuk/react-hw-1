import { favItemsTypes } from "../types";

export function addToFavItems(products, id) {
   return {
      type: favItemsTypes.ADD_TO_FAV,
      payload: { products, id },
   }
}
export function removeFromFavItems(id) {
   return {
      type: favItemsTypes.DEL_FROM_FAV,
      payload: id,
   }
}