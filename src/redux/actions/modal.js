import { modalTypes } from "../types";

export function toogleModal() {
   return {
      type: modalTypes.TOOGLE_MODAL
   }
}