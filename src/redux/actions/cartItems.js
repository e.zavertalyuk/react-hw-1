
import { cartItemsTypes } from "../types";

export function addToCartItems(id) {
   return {
      type: cartItemsTypes.ADD_TO_CART,
      payload: id,
   }
}
export function removeFromCartItems(item) {
   return {
      type: cartItemsTypes.DEL_FROM_CART,
      payload: item,
   }
}
export function clearCart(values) {
   return {
      type: cartItemsTypes.CLEAR_CART,
      payload: values
   }
}