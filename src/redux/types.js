export const productTypes = {
   FILL_PRODUCTS: "FILL_PRODUCTS"
}
export const modalTypes = {
   TOOGLE_MODAL: "TOOGLE_MODAL",
}
export const cartItemsTypes = {
   ADD_TO_CART: "ADD_TO_CART",
   DEL_FROM_CART: "DEL_FROM_CART",
   CLEAR_CART: "CLEAR_CART"
}

export const favItemsTypes = {
   ADD_TO_FAV: "ADD_TO_FAV",
   DEL_FROM_FAV: "DEL_FROM_FAV"
}
export const selectedItemTypes = {
   SELECT_ITEM: "SELECT_ITEM",
   REMOVE_ITEM: "REMOVE_ITEM"
}