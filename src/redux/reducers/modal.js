import { modalTypes } from "../types";

const initialState = {
   openModal: false
}

export function modalReducer(state = initialState, action) {
   switch (action.type) {
      case modalTypes.TOOGLE_MODAL:
         return {
            ...state,
            openModal: !state.openModal
         }

      default:
         return state
   }
}
