import { cartItemsTypes } from "../types";

const initialCartItems = localStorage.getItem("cartItems");
const initialState = {
   cartItems: initialCartItems ? JSON.parse(initialCartItems) : []
}

export function cartReducer(state = initialState, action) {
   switch (action.type) {
      case cartItemsTypes.ADD_TO_CART:
         const cartItemsArr = [...state.cartItems, action.payload]
         localStorage.setItem("cartItems", JSON.stringify(cartItemsArr));
         return {
            ...state,
            cartItems: cartItemsArr,
         }
      case cartItemsTypes.DEL_FROM_CART:
         const deletingElIndex = state.cartItems.findIndex((item) => item.id === action.payload.id);
         const newCartArr = state.cartItems.filter((item, index) => index !== deletingElIndex);
         localStorage.setItem("cartItems", JSON.stringify(newCartArr));
         return {
            ...state,
            cartItems: newCartArr
         };
      case cartItemsTypes.CLEAR_CART:
         console.log(console.log('Данные для отправки:', action.payload, 'Купленные товары:', state.cartItems));
         localStorage.removeItem("cartItems");
         return {
            ...state,
            cartItems: []
         }
      default:
         return state
   }
}
