// Core
import { combineReducers } from "redux";
import { productsReducer as products } from "./reducers/products";
import { modalReducer as modal } from "./reducers/modal";
import { cartReducer as cart } from "./reducers/cartItems";
import { favReducer as fav } from "./reducers/favItems";
import { selectItem } from "./reducers/selectedItem";
// Reducers

export const rootReducer = combineReducers({ products, modal, cart, fav, selectItem })