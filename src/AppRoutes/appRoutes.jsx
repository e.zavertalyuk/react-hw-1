import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import FavItems from "../pages/FavItems";
import CartItems from "../pages/CartItems";

export function AppRoutes({ handleClickAddToChart, handleClickAddToChartInCart, handleRemoveToCart, delBtn, }) {

   return (
      <Routes>
         <Route path="/" element={<Home
            removeFromCart={handleRemoveToCart}
            addToCart={handleClickAddToChart}
         />
         } />
         <Route path="/cart" element={<CartItems
            addToCart={handleClickAddToChartInCart}
            removeFromCart={handleRemoveToCart}

         />} />
         <Route path="/favourite" element={<FavItems
            removeFromCart={handleRemoveToCart}
            addToCart={handleClickAddToChart}
         />} />
      </Routes>
   )
}