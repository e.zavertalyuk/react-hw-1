import * as Yup from "yup"

export const usersSchema = Yup.object({

   firstName: Yup.string()
      .min(3, 'Min 3 letters')
      .max(20, 'Only 20 letters')
      .required('This field is required'),

   lastName: Yup.string()
      .min(5, 'Min 5 letters')
      .max(20, 'Only 20 letters')
      .required('This field is required'),

   userAge: Yup.number()
      .min(18, 'Must be at least 18 years old')
      .required('This field is required'),

   delivery: Yup.string()
      .min(5, 'Min 5 letters')
      .max(20, 'Only 20 letters')
      .required('This field is required'),
   phoneNumber: Yup.string()
      .min(10, 'Write a correct number ')
      .max(20, 'Write a correct number ')
      .required('This field is required'),
})